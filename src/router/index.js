import Vue from 'vue'
import VueRouter from 'vue-router'
import Padre from '../views/Components.vue'
import Baraja from '../views/Baraja.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/Tabla',
    name: 'Tabla',
    component: Padre
  },
  {
    path: '/Baraja',
    name: 'Baraja',
    component: Baraja
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
